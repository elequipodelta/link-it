<%--
    Document   : user
    Created on : 11-sep-2019, 17:15:57
    Author     : mtt99
 +${deck.value.id}
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../CSS/user.css" rel="stylesheet" type="text/css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="../JS/user.js"></script>
    </head>
    <body>
        <div id="tittle">
            <h1>Link-it</h1>
        </div>
        <div id="dialog-form" title="Create new deck">
            <form id="frmNewUsr" action="/deck" method="post">
                <fieldset>
                    <label for="title">title</label>
                    <input type="text" name="title" id="title" placeholder="title"><br>
                    <!-- Allows form submission from the keyboard without duplicating the dialog button -->
                    <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                </fieldset>
            </form>
        </div>
        <div id="dialog-formDelete" title="Delete deck">
            <form id="deleteDeckForm" action="/deck" method="post">
                <fieldset>
                    <label for="title">Title: </label>
                    <select name="did">
                        <c:forEach var="deck" items="${user.decks}">
                            <option value="${deck.key}">
                                ${deck.value}
                            </option>
                        </c:forEach>
                    </select>
                    <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                </fieldset>
            </form>
        </div>
        <div id="lateralMenu">
            <button id="create-deck">Create new deck</button>
            <button id="delete-deck">Delete deck</button>
            <div id="decks">
                <c:forEach var="deck" items="${user.decks}">

                    <div class="deck" id="${deck.key}">
                        ${deck.value}
                    </div>

                </c:forEach>
            </div>
            <form id="edit-deck" action="/deck" method="GET">
                <input type="submit" value="edit">
                <input type="hidden" id="did" value="" name="did" >
            </form>

        </div>



        <div id="main">

        </div>
        <script>
            $(".deck").click(function () {
                $("#main").load("./deck?did=" + event.target.id);
                $("#did").val(event.target.id);
            });
            setUid("${user.id}");

        </script>
    </body>
</html>
