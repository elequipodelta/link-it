var uid;

function setUid(uidstr) {
    uid = uidstr;
}
$(function () {
    var dialog;
    var form;
   
    var dialogDelete;
    var formDelete;
   
//adds the user to the DB
    function addDeck() {
        $.post("/deck", "action=AD&uid=" + uid + '&' + $('#frmNewUsr').serialize());
        dialog.dialog("close");
    }
        function deleteDeck() {
        $.post("/deck", "action=DD&uid=" + uid + '&' + $('#deleteDeckForm').serialize());
        dialogDelete.dialog("close");
    }
//turns dialog-form div into a dialog
    dialog = $("#dialog-form").dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "Add deck": function () {
                addDeck();
                $('body').removeClass('is-dimmed');
            },
            Cancel: function () {
                $('body').removeClass('is-dimmed');
                dialog.dialog("close");
            }
        }});

    //Cancels default form sumbit and replaces it for addUser()
    form = dialog.find("form").on("submit", function (event) {
        event.preventDefault();
        addDeck();
    });
    //Shows dialog on button press
    $("#create-deck").button().on("click", function () {
        $('body').addClass('is-dimmed');
        dialog.dialog("open");
    });
    
    
    dialogDelete = $("#dialog-formDelete").dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "Delete deck": function () {
                deleteDeck();
                $('body').removeClass('is-dimmed');
            },
            Cancel: function () {
                $('body').removeClass('is-dimmed');
                dialogDelete.dialog("close");
            }
        }});

    //Cancels default form sumbit and replaces it for addUser()
    formDelete = dialogDelete.find("form").on("submit", function (event) {
        event.preventDefault();
        deleteDeck();
    });
    //Shows dialog on button press
    $("#delete-deck").button().on("click", function () {
        $('body').addClass('is-dimmed');
        dialogDelete.dialog("open");
    });
});

