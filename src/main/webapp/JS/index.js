$(function () {
    var dialog;
    var form;
//adds user to the DB
    function addUser() {
        $.post("/user",$('form').serialize());
        dialog.dialog("close");
    }
//turns dialog-form div into a dialog 
    dialog = $("#dialog-form").dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "Create an account": function () {
                addUser();
                $('body').removeClass('is-dimmed');
            },
            Cancel: function () {
                $('body').removeClass('is-dimmed');
                dialog.dialog("close");
            }
        }});

    //Cancels default form sumbit and replaces it for addUser()   
    form = dialog.find("form").on("submit", function (event) {
        event.preventDefault();
        addUser();
    });
    //shows dialog on button press
    $("#create-user").button().on("click", function () {
        $('body').addClass('is-dimmed');
        dialog.dialog("open");
    });
});