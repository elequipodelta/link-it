var selectedElement;
var offset;
var linkList = [];
var linkStrList = [];
var coords = [];
var did;
var link;
var c1;
var c2;
var dragState;
const dragStates = {
    NODRAG: "nodrag",
    CARDDRAG: "carddrag",
    LINKDRAG: "linkdrag"
}

function setDid(didstr) {
    did = didstr;
}
function setLink(cLinkStr) {
    link = cLinkStr;
}
$(function () {
    var dialogLink;
    var formLink;
    var dialogCard;
    var formCard;
    var formDeleteCard;
    var dialogDeleteCard;
//adds card to DB
    function addCard() {
        $.post("/card", "action=AD&did=" + did + "&" + $('form#addCard').serialize());
        dialogCard.dialog("close");
    }
    function addLink() {
        $.post("/deck", "action=AL&did=" + did + '&' + $('form#addLink').serialize());
        dialogLink.dialog("close");
    }
    function deleteCard(){
         $.post("/card", "action=DEL&did=" + did + "&" + $('form#deleteCard').serialize());
        dialogDeleteCard.dialog("close");
    }
//turns dialog-form div into a dialog
    dialogCard = $("#dialog-form-card").dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "Create card": function () {
                addCard();
                $('body').removeClass('is-dimmed');
            },
            Cancel: function () {
                $('body').removeClass('is-dimmed');
                dialogCard.dialog("close");
            }
        }});

    //Cancels default form sumbit and replaces it for addUser()
    formCard = dialogCard.find("form").on("submit", function (event) {
        event.preventDefault();
        addCard();
    });
    //Shows dialog on button press
    $("#create-card").button().on("click", function () {
        $('body').addClass('is-dimmed');
        dialogCard.dialog("open");
    });
    dialogLink = $("#dialog-form-link").dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "Add link": function () {
                addLink();
                $('body').removeClass('is-dimmed');
            },
            Cancel: function () {
                $('body').removeClass('is-dimmed');
                dialogLink.dialog("close");
            }
        }});

//Cancels default form sumbit and replaces it for addLink()

    formLink = dialogLink.find("form").on("submit", function (event)
    {
        event.preventDefault();
        addLink();
    });
//Shows dialog on button press
    $("#create-link").button().on("click", function ()
    {
        $('body').addClass('is-dimmed');
        dialogLink.dialog("open");
    });
    //////////////////////////////////////////////////////////////////////////////////
    
    dialogDeleteCard = $("#dialog-form-deleteCard").dialog({
        autoOpen: false,
        modal: true,
        buttons: {
            "Delete card": function () {
                deleteCard();
                $('body').removeClass('is-dimmed');
            },
            Cancel: function () {
                $('body').removeClass('is-dimmed');
                dialogDeleteCard.dialog("close");
            }
        }});

    //Cancels default form sumbit and replaces it for addUser()
    formDeleteCard = dialogDeleteCard.find("form").on("submit", function (event) {
        event.preventDefault();
        deleteCard();
    });
    //Shows dialog on button press
    $("#delete-card").button().on("click", function () {
        $('body').addClass('is-dimmed');
        dialogDeleteCard.dialog("open");
    });
    
    
    
    
    //////////////////////////////////////////////////////////////////////////////////   
    
    
});
window.onload = function ()
{
    // Make the DIV element draggable:
    makeDraggable(document.getElementById("board"));
    drawLinks(generateLinkCoords());
    //setInterval(update, 0.1);

};
function update(evt)
{
    deleteLinks();
    drawLinks(generateLinkCoords(evt));
}

function makeDraggable(svg) {
    svg.addEventListener('mousedown', startDrag);
    svg.addEventListener('mousemove', drag);
    svg.addEventListener('mouseup', endDrag);
    svg.addEventListener('mouseleave', endDrag);
}

function getCartInfo(evt) {
    var element = document.createElementNS('http://www.w3.org/2000/svg', 'text');
    element.setAttributeNS(null, 'x', 5);
    element.setAttributeNS(null, 'y', 15);
    var txt = document.createTextNode("Hello World");
    element.appendChild(txt);
    evt.target.appendChild(element);
}


function drag(evt) {

    if (dragState == dragStates.CARDDRAG) {
        evt.preventDefault();
        var coord = getMousePosition(evt);
        selectedElement.setAttributeNS(null, "x", coord.x - offset.x);
        selectedElement.setAttributeNS(null, "y", coord.y - offset.y);
    } else if (dragState == dragStates.LINKDRAG) {


    }

    update(evt);

}


function deleteLinks() {
    var parent = document.getElementById("board");
    var lines = document.getElementsByClassName("line");
    var tacks = document.getElementsByClassName("tack");
    Array.prototype.forEach.call(lines, function (entry) {
        parent.removeChild(entry);
    });
    Array.prototype.forEach.call(tacks, function (entry) {
        parent.removeChild(entry);
    });
}

function startDrag(evt) {

    if (evt.target.classList.contains('circle')) {

        dragState = dragStates.LINKDRAG
        c1 = event.target.parentNode.id;
    } else if (evt.target.parentNode.classList.contains('draggable')) {
        dragState = dragStates.CARDDRAG;
        selectedElement = evt.target.parentNode;
        offset = getMousePosition(evt);
        offset.x -= parseFloat(selectedElement.getAttributeNS(null, "x"));
        offset.y -= parseFloat(selectedElement.getAttributeNS(null, "y"));
    }
}

function endDrag(evt) {
    selectedElement = null;
    if (dragState == dragStates.LINKDRAG && evt.target.classList.contains('circle')) {
        c2 = evt.target.parentNode.id;
        var link = c1 + "-" + c2;
        if (getLinkStr().includes(link) || getLinkStr().includes(c2 + "-" + c1)) {
            console.log("rl: " + link)
            removeLink(link)

            $.post("/deck", "action=RL&did=" + did + '&card1=' + c1 + '&card2=' + c2);
        } else {
            console.log("al: " + link)
            addLink(link);
            $.post("/deck", "action=AL&did=" + did + '&card1=' + c1 + '&card2=' + c2);
        }


    }
    dragState = dragStates.NODRAG;
    update(evt);
}


function getMousePosition(evt) {
    var CTM = document.getElementById("board").getScreenCTM();
    return {
        x: (evt.clientX - CTM.e) / CTM.a,
        y: (evt.clientY - CTM.f) / CTM.d
    };
}

function getCenterPoint(idCard) {
    var el = document.getElementById(idCard);
    if (el === null) {
//        alert(idCard)
        return null;
    }
    var bbox = el.getBBox();
    var ctm = document.getElementById(idCard).getCTM();
// Calculate the centre of the group
    var cx = bbox.x + bbox.width / 2;
    var cy = bbox.y + 25;
    var pt = document.getElementById("board").createSVGPoint();
    pt.x = cx;
    pt.y = cy;
    pt = pt.matrixTransform(ctm);
    return pt;
}

function generateLinkCoords(evt) {
    coords = [];
    linkList.forEach(function (entry) {
        if (entry.c1 === null || entry.c2 === null) {
            return;
        }

        var posA = getCenterPoint(entry.c1);
        var posB = getCenterPoint(entry.c2);
        coords.push({posA: posA, posB: posB});
    });
    if (dragState == dragStates.LINKDRAG) {
        var posA = getCenterPoint(c1);
        var posB = getMousePosition(evt);
        posB.y = posB.y - 10;
        coords.push({posA: posA, posB: posB});
    }
    return coords;
}

function drawLinks(linkCoords) {

    linkCoords.forEach(function (entry) {
        if (entry.posA === null || entry.posB === null) {
            return null;
        }
        drawLine(entry.posA, entry.posB);
        drawTack(entry.posA);
        drawTack(entry.posB);
    });
}

function drawTack(point) {
    var newTack = document.createElementNS('http://www.w3.org/2000/svg', 'image');
    newTack.setAttribute('class', 'tack');
    newTack.setAttribute('id', point.x + " " + point.y)
    newTack.setAttribute('x', point.x - 9);
    newTack.setAttribute('y', point.y - 30);
    newTack.setAttribute("height", "35");
    newTack.setAttribute("width", "26");
    newTack.setAttribute("href", "../IMG/xinxeta.png");
    document.getElementById("board").appendChild(newTack);
}

function addLink(linkStr) {
    var cards = linkStr.split("-");
    linkList.push({
        c1: cards[0],
        c2: cards[1]
    });
    linkStrList.push(linkStr);
}

function removeLink(linkStr) {
    var index = linkStrList.indexOf(linkStr)
    if (index == -1) {
        var reves = linkStr.split("-");
        index = linkStrList.indexOf(reves[1] + "-" + reves[0]);
    }
    linkList.splice(index, 1)
    linkStrList.splice(index, 1)
}

function getLinkStr() {
    var str = "";
    linkList.forEach(function (link) {
        str = str + link.c1 + "-" + link.c2 + "\n"

    })
    console.log(str)
    return str;
}


function drawLine(pointA, pointB) {

    var newLine = document.createElementNS('http://www.w3.org/2000/svg', 'line');
    newLine.setAttribute('class', 'line');
    newLine.setAttribute('x1', Number(pointA.x));
    newLine.setAttribute('y1', Number(pointA.y));
    newLine.setAttribute('x2', Number(pointB.x));
    newLine.setAttribute('y2', Number(pointB.y));
    newLine.setAttribute("style", "stroke:rgb(255,0,0);stroke-width:2");
    document.getElementById("board").appendChild(newLine);
}
