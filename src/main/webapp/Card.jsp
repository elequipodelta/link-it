<%--
    Document   : Card
    Created on : 10-sep-2019, 12:22:39
    Author     : Aimnox
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Card</title>
        <link rel="stylesheet" type="text/css" href="./CSS/Card.css"/>
    </head>
    <body>
        <div id="card">
            <div class="card">
                <div class="cardheader" name="title"><p id="center"></p></div>
                <div class="cardbody" name="text"><p id="description"></p></div>
            </div>
        </div>
    </body>
</html>
