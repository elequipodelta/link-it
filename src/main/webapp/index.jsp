<%--
    Document   : index
    Created on : 10-sep-2019, 12:29:03
    Author     : Aimnox
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href="./CSS/index.css" rel="stylesheet" type="text/css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="../JS/index.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    </head>
    <body>
        <div id="header">

            <!--Inserta header.html-->
        </div>
        <div id="dialog-form" title="Create new user">
            <form id="frmNewUsr" action="/user" method="post">
                <fieldset>
                    <label for="name">Username</label>
                    <input type="text" name="name" id="name" placeholder="username"><br>
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" placeholder="email" ><br>
                    <!-- Allows form submission from the keyboard without duplicating the dialog button -->
                    <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
                </fieldset>
            </form>
        </div>
        <div id="indice">
            <form action = "/user" method = "GET">
                <b id="texto">Username:  <input type = "text" name = "u" ></b>
                <input type = "submit" value = "Submit" />
            </form>
        </div>
        <button id="create-user">Create new user</button>

    </body>
</html>
