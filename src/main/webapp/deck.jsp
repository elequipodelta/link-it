<%--
    Document   : deck
    Created on : 10-sep-2019, 12:30:34
    Author     : Aimnox
--%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="../CSS/Deck.css"></link>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="../JS/deck.js"></script>


    </head>
    <body>
        <div>
            <h1>${deck.title}</h1>
        </div>
        <div class="dialog" id="dialog-form-card" title="Create new card">
            <form id="addCard" action="/card" method="post">
                <fieldset>
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title" placeholder="title"></input><br/>
                    <label for="desc">Text</label>
                    <textArea  name="desc" id="desc" placeholder="Texto"></textArea><br/>
                    <!-- Allows form submission from the keyboard without duplicating the dialog button -->
                    <input type="submit" tabindex="-1" style="position:absolute; top:-1000px"></input>
                </fieldset>
            </form>
        </div>
        <div class="dialog" id="dialog-form-link" title="Create new card">
            <form id="addLink" action="/link" method="post">
                <fieldset>
                    <label for="card1">Card 1</label>
                    <select name="card1">
                        <c:forEach items="${cartas}" var="card">
                            <option value="${card.id}">
                                ${card.title}
                            </option>
                        </c:forEach>
                    </select>
                    <label for="card2">Card 2</label>
                    <select name="card2">
                        <c:forEach items="${cartas}" var="card">
                            <option value="${card.id}">
                                ${card.title}
                            </option>
                        </c:forEach>
                    </select>
                    <input type="submit" tabindex="-1" style="position:absolute; top:-1000px"></input>
                </fieldset>
            </form>
        </div>
        <div class="dialog" id="dialog-form-deleteCard" title="delete a card">
            <form id="deleteCard" action="/deleteCard" method="post">
                <fieldset>
                    <label for="cardId">Card</label>
                    <select name="cardId">
                        <c:forEach items="${cartas}" var="card">
                            <option value="${card.id}">
                                ${card.title}
                            </option>
                        </c:forEach>
                    </select>
                    <input type="submit" tabindex="-1" style="position:absolute; top:-1000px"></input>
                </fieldset>
            </form>
        </div>
        
        <button id="create-card">Create new card</button>
        <button id="create-link">Create link</button>
        <button id="delete-card">Delete card</button>
        <div id="boardDiv" z="0">
            <svg  id="board">
                <g id="cards" display="grid">
            <c:forEach items="${cartas}" var="card" varStatus="vs">
                <c:set var="posX" value="${vs.index%5}" ></c:set>
                <fmt:formatNumber var="posY" maxFractionDigits="0" value="${vs.index/5}"></fmt:formatNumber>

                        <svg id="${card.id}" class="draggable"
                         x="${posX*220+20}" y="${posY*320+20}"
                         width="210" height="300">

                            <rect
                                id="rectTitle"
                                width="210"
                                height="50"
                                x="0"
                                y="0"
                                style="fill:#BEA637;stroke-width:0.23330867" />
                            <rect
                                style="fill:#BEB68F;stroke-width:0.27371967"
                                id="rectDesc"
                                width="210"
                                height="250"
                                x="0"
                                y="50" />
                            <circle
                                cx="210"
                                cy="0"
                                r="25"
                                style="fill:#FFFFFF"
                                class="circle"/>
                            <foreignObject x="5" y="5" width="170" height="40" id="textTitle">
                                <p xmlns="http://www.w3.org/1999/xhtml">${card.title}</p>
                            </foreignObject>
                            <foreignObject x="5" y="65" width="200" height="200" id="textTitle  ">
                                <p xmlns="http://www.w3.org/1999/xhtml">${card.desc}</p>
                            </foreignObject>
                        </svg>
            </c:forEach>
                </g>
            </svg>
        </div>
        <script>
            '${links}'.split("/").forEach(function (entry) {
                addLink(entry);
            });
            setDid("${deck.id}");
            setLink("${card.id}");
        </script>


    </body>
</html>
