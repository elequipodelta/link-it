package com.equipodelta.link.it.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Card {

    private String title;
    private String id;
    private String desc;
    private transient Deck deck;
    private List<String> alias;
    private CardType cardType;

    public Card() {
        alias = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String text) {
        this.desc = text;
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public List<String> getAlias() {
        return alias;
    }

    public void setAlias(List<String> alias) {
        this.alias = alias;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + (this.title != null ? this.title.hashCode() : 0);
        hash = 19 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 19 * hash + (this.desc != null ? this.desc.hashCode() : 0);
        hash = 19 * hash + (this.deck != null ? this.deck.hashCode() : 0);
        hash = 19 * hash + (this.alias != null ? this.alias.hashCode() : 0);
        hash = 19 * hash + (this.cardType != null ? this.cardType.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Card other = (Card) obj;
        if ((this.title == null) ? (other.title != null) : !this.title.equals(other.title)) {
            return false;
        }
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        if ((this.desc == null) ? (other.desc != null) : !this.desc.equals(other.desc)) {
            return false;
        }
        if (this.deck != other.deck && (this.deck == null || !this.deck.equals(other.deck))) {
            return false;
        }
        if (this.alias != other.alias && (this.alias == null || !this.alias.equals(other.alias))) {
            return false;
        }
        if (this.cardType != other.cardType) {
            return false;
        }
        return true;
    }

    public void setSerializedAlias(String aliases) {
        setAlias(Arrays.asList(aliases.split("/")));
    }

    public String getSerializedAlias() {
        StringBuilder sb = new StringBuilder();
        for (String s : getAlias()) {
            sb.append(s);
            sb.append("/");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

}
