/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equipodelta.link.it.models;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author Aimnox
 */
public class User {

    private String id;
    private String username;
    private String email;
    private Map<String, Deck> decks;
    private int lastDeckId;
    private static final int DECK_AMOUNT_LIMIT = 10;

    public User() {
        decks = new HashMap<>();
        lastDeckId = 0;
        username="SEÑOR GRIS";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, Deck> getDecks() {
        return decks;
    }

    public void setDecks(Map<String, Deck> decks) {
        this.decks = decks;
    }

    //CRUD
    //Create
    public Deck addDeck(Deck deck) {
        if (deck.getId() == null) {
            deck.setId(generateDeckId());
            System.out.println(deck.getId() + decks.size());
        }
        if (decks.containsKey(deck.getId())) {
            System.out.println("Error: deck already exist");
            return null;
        }
        if (deckCount() >= DECK_AMOUNT_LIMIT) {
            return null; //TODO: Mooolt guarro
        }
        deck.setUser(this);
        return decks.put(deck.getId(), deck);
    }

    //Read
    public Deck getDeck(String id) {
        return decks.get(id);
    }

    //Update
    public Deck updateDeck(Deck deck) {
        return decks.replace(deck.getId(), deck);
    }

    //Delete
    public Deck deleteDeck(String id) {
        return decks.remove(id);
    }

    public String generateDeckId() {
        return String.format("%sd%d", this.id, lastDeckId++);

    }

    public int deckCount() {
        return decks.size();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.username);
        hash = 89 * hash + Objects.hashCode(this.decks);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        if (!Objects.equals(this.decks, other.decks)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username=" + username + ", email=" + email + '}';
    }
    

    public void printDecks() {
        for (Deck d : decks.values()) {
            System.out.printf("Deck %s:\n", d.getId());
            d.printDeck();
        }
    }
}
