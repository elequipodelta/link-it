package com.equipodelta.link.it.models;

import java.util.Objects;

public class Link {

    private Card card1;
    private Card card2;

    public Link() {

    }

    public Link(Card c1, Card c2) {
        this.card1 = c1;
        this.card2 = c2;
    }

    public Card getCard1() {
        return card1;
    }

    public void setCard1(Card card1) {
        this.card1 = card1;
    }

    public Card getCard2() {
        return card2;
    }

    public void setCard2(Card card2) {
        this.card2 = card2;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.card1);
        hash = 29 * hash + Objects.hashCode(this.card2);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Link other = (Link) obj;
        return (other.getCard1().getId() == card1.getId() && other.getCard2().getId() == card2.getId())
                || (other.getCard1().getId() == card2.getId() && other.getCard2().getId() == card1.getId());
    }

    @Override
    public String toString() {
        if (card1 == null || card2 == null) {
            return "ERROR";
        }
        return card1.getId() + "-" + card2.getId();
    }

}
