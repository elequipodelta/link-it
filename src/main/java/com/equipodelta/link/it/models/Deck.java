package com.equipodelta.link.it.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;
import java.util.List;
import java.util.Map;

public class Deck {

    private String id;
    private transient User user;
    private Map<String, Card> cards;
    private List<Link> links;
    private String serializedLinks;
    private int lastCardId;
    private String title;

    public Deck() {
        cards = new HashMap<>();
        links = new ArrayList<>();
        lastCardId = 0;
        title = "";
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Map<String, Card> getCards() {
        return cards;
    }

    public void setCards(Map<String, Card> cards) {
        this.cards = cards;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public Card addCart(Card card) {
        if (card.getId() == null) {
            card.setId(generateCardCode());
        }
        if (cards.containsKey(card.getId())) {
            System.out.println("Error: card already exist");
            return null;
        }
        links.addAll(generateLinks(card));
        card.setDeck(this);
        return cards.put(card.getId(), card);
    }

    public Card getCard(String id) {
        return cards.get(id);
    }

    public Card updateCard(Card card) {
        return cards.replace(card.getId(), card);
    }

    public Card deleteCard(String id) {
        return cards.remove(id);
    }

    public String generateCardCode() {
        return String.format("%sc%d", this.id, lastCardId++);
    }

    public String[] getAliases() {
        List<String> aliases = new ArrayList<>();
        for (Card c : cards.values()) {
            aliases.add(c.getTitle());
            aliases.addAll(c.getAlias());
        }
        return aliases.toArray(new String[0]);
    }

    public List<Link> generateLinks(Card card) {
        List<Link> list = new ArrayList<>();
        String texto = card.getDesc();

        texto = texto == null ? "" : texto;
        for (String alias : getAliases()) {
            if (texto.contains(alias)) {
                list.add(new Link(card, getCardByName(alias)));
            }
        }
        return list;
    }

    public List<Link> generateAllLinks() {
        List<Link> list = new ArrayList<>();
        for (Card c : cards.values()) {
            list.addAll(generateLinks(c));
        }
        return list;
    }

    public Card getCardByName(String nombre) {
        for (Card c : cards.values()) {
            if (c.getTitle().equals(nombre) || c.getAlias().contains(nombre)) {
                return c;
            }
        }
        return null;
    }

    public boolean removeLink(Link link) {
        Link l = null;
        for (Link lk : links) {
            if (lk.equals(link)) {
                l = lk;
                break;
            }
        }
        if (l != null) {
            links.remove(l);
            serializedLinks = getSerializedLinks();
            return true;
        }
        return false;

    }

    public boolean removeLink(String str) {
        String cards[] = str.split("-");
        return removeLink(new Link(getCard(cards[0]), getCard(cards[1])));
    }

    public boolean addLink(Card c1, Card c2) {
        Link link = new Link(c1, c2);
        if (getSerializedLinks().contains(link.toString())) {
            return false;
        }
        return links.add(link);
    }

    public boolean addLink(String c1, String c2) {
        return addLink(getCard(c1), getCard(c2));
    }

    public boolean addLink(String linkstr) {
        String[] strs = linkstr.split("-");
        return addLink(strs[0], strs[1]);

    }

    public String getSerializedLinks() {
        if (links.size() == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (Link l : links) {
            sb.append(l.toString());
            sb.append("/");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    public boolean loadSerializedLinks() {
        if (serializedLinks.equals("")) {
            return true;
        }
        String[] lista = serializedLinks.split("/");
        for (String str : lista) {
            
            if (str.matches(".*-.*") && !addLink(str)) {
                return false;
            }

        }
        return true;
    }

    public void setSerializedLinks(String serializedLinks) {
        this.serializedLinks = serializedLinks;
    }

    public void printDeck() {

        System.out.println("Deck with " + cards.size() + " cards");
        for (Card c : cards.values()) {
            System.out.printf("%s: %s\n", c.getId(), c.getTitle());
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.id);
        hash = 37 * hash + Objects.hashCode(this.user);
        hash = 37 * hash + Objects.hashCode(this.cards);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Deck other = (Deck) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.cards, other.cards)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return title;
    }

}
