package com.equipodelta.link.it;

import com.equipodelta.link.it.dao.CartasDAO;
import com.equipodelta.link.it.dao.Conection;
import com.equipodelta.link.it.dao.DAO;
import com.equipodelta.link.it.dao.GsonHandler;
import com.equipodelta.link.it.dao.UserDAO;
import com.equipodelta.link.it.models.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public abstract class Main {

    private static final String JSON_TEST_FILE = "/home/sv/Escritorio/Test.json";

    public static void main(String[] args) {

    }

    private static void test1() throws Exception {
        //Creates a user
        User usuario1 = new User();
        usuario1.setId("U1");
        
        //Creates a deck
        Deck deck1 = new Deck();
        // deck1.setId(usuario1.generateDeckId());
        
        // and adds it a deck
        usuario1.addDeck(deck1);
        // a�adirle cartas
        for (int i = 0; i < 10; i++) {
            Card card = new CardText();
            // card.setId(deck1.generateCardCode());
            card.setTitle("Carta " + i);
            deck1.addCart(card);
        }
        deck1.addLink("U1d0c4", "U1d0c1");
        deck1.addLink("U1d0c4", "U1d0c5");
        deck1.addLink("U1d0c4", "U1d0c7");
        deck1.addLink("U1d0c2", "U1d0c1");

        User nuevoUsuario = new User();
        nuevoUsuario.setUsername("John");
        nuevoUsuario.setEmail("john@cena.wwe");
        nuevoUsuario.setId("U23");
        nuevoUsuario.setDecks(nuevoUsuario.getDecks());

        DAO<User> userDao = new UserDAO();
        userDao.insert(nuevoUsuario);

        try {
            GsonHandler.toFile(usuario1, JSON_TEST_FILE);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        deck1.printDeck();
        List<User> usua = new UserDAO().getAll();
        for (User u : usua) {
            System.out.println(u);
        }

    }

}
