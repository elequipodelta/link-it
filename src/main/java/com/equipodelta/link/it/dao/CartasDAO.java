package com.equipodelta.link.it.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.equipodelta.link.it.models.Card;
import com.equipodelta.link.it.models.CardText;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Child class from the generic DAO class. It contains CRUD code logic for the card itself.
 * @author sv
 */
public class CartasDAO extends DAO<Card> {

    public CartasDAO() {
        super("cards", "ID_CARD", "(`ID_CARD`, `ID_DECK`, `TITULO`, `TEXTO`)");
    }

    @Override
    protected Card build(ResultSet rs) throws SQLException {
        Card card = new CardText();
        card.setId(rs.getString("ID_CARD"));
        card.setTitle(rs.getString("TITULO"));
        card.setDesc(rs.getString("TEXTO"));
        card.setAlias(new ArrayList<String>());

        return card;
    }

    @Override
    protected String[] deconstruct(Card c) {
        String[] atr = new String[4];
        atr[0] = c.getId();
        atr[1] = c.getDeck().getId();
        atr[2] = c.getTitle();
        atr[3] = c.getDesc();
        //atr[4]=c.getCardType().toString();
        return atr;
    }

    public List<Card> getCartasByDeck(String deckID) {

        try {
            List<Card> cards = buildList(query("SELECT * FROM cards WHERE ID_DECK=" + "\"" + deckID + "\""));
            return cards;
        } catch (SQLException ex) {
            System.out.println("Fallo en cartas by deck");
            System.out.println(ex);
            Logger.getLogger(CartasDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<Card>();
    }

    public String[] getAliasByCardID(String cardID) throws SQLException {
        Statement stm = null;
        ResultSet rs = null;

        stm = con.createStatement();

        String query = "SELECT * FROM ALIAS WHERE ID_CARD=" + "\"" + cardID + "\"";
        rs = stm.executeQuery(query);
        rs.next();
        String str = rs.getString("ALIAS");

        String[] strA = str.split("-");

        return strA;
    }

    public void setAliasByCardID(List<String> aliasList, String cardID) throws SQLException {
        ResultSet rs = (query("SELECT * FROM ALIAS WHERE ID_CARD=" + "\"" + cardID + "\""));
        List<String> str = Arrays.asList(rs.getString("ID_CARD").split("-"));
        List<String> finalList = new ArrayList<>();
        for (String alias : aliasList) {
            if (!str.contains(alias)) {
                finalList.add(alias);
            }
            aliasInsert(finalList, cardID);
        }
    }

    public void aliasInsert(List<String> finalList, String cardID) {

        try {
            Statement stm = stm = con.createStatement();
            String str = String.join("-", finalList);
            stm.executeUpdate(String.format("UPDATE `linkitdatabase`.`alias` SET `ALIAS` = '%s' WHERE(`ID_CARD` = '%s')", str, cardID));
        } catch (SQLException ex) {
            Logger.getLogger("ha fallado el update de los alias");
        }
    }

    public String generateCode(String deckId, int i) {
        Random rdm = new Random();
        String code = deckId + "c" + (Math.abs(rdm.nextInt()) % 10000);
        if (!exist(code)) {
            return code;
        } else {
            if (i > 100) {
                return null;
            }
            return generateCode(deckId, ++i);
        }
    }

}
