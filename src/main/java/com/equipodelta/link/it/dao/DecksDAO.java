package com.equipodelta.link.it.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import com.equipodelta.link.it.models.Deck;
import com.equipodelta.link.it.models.User;
import java.sql.Statement;
import java.util.List;
import java.util.Random;

public class DecksDAO extends DAO<Deck> {

    public DecksDAO() {
        super("decks", "ID_DECK", "(`ID_USER`,`TITULO`,`ID_DECK`,`LINKS`)");
        // TODO Auto-generated constructor stub
    }

    @Override
    protected Deck build(ResultSet rs) throws SQLException {
        // TODO Auto-generated method stub
        Deck deck = new Deck();
        User usuario = new User();
        usuario.setId(rs.getString("ID_USER"));
        deck.setUser(usuario);
        deck.setId(rs.getString("ID_DECK"));
        deck.setTitle(rs.getString("TITULO"));
        deck.setSerializedLinks(rs.getString("LINKS"));
        return deck;
    }

    @Override
    protected String[] deconstruct(Deck d) {
        String[] atr = new String[4];
        atr[0] = d.getUser().getId();
        atr[1] = d.getTitle();
        atr[2] = d.getId();
        atr[3] = d.getSerializedLinks();
        return atr;
    }

    public List<Deck> getDeckByUser(String userID) {
        try {
            return buildList(query("SELECT * FROM DECKS WHERE ID_USER=" + "\"" + userID + "\""));
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return null;
    }

    public String generateCode(String userCode) {
        //Podria ser peor la verdad🤷
        Random rdm = new Random();
        String code = userCode + "d" + (Math.abs(rdm.nextInt()) % 10000);
        if (!exist(code)) {
            return code;
        } else {
            return generateCode(userCode);
        }
    }

    @Override
    public boolean update(Deck o) {
        System.out.println("Update deck:");
        StringBuilder sb = new StringBuilder();
        for (String s : deconstruct(o)) {
            sb.append("\", \"" + s);
        }
        String[] prm = deconstruct(o);
        String query = String.format(
                "UPDATE `linkitdatabase`.`decks` "
                + "SET `ID_DECK` = \"%s\",`ID_USER` = \"%s\", `TITULO` = \"%s\", `LINKS` = \"%s\""
                + " WHERE (`ID_DECK`=\"%S\")",
                //(SELECT ID FROM decks WHERE id_deck=\"%s\");",
                prm[2], prm[0], prm[1], prm[3], prm[2]);
        System.out.println(query);
        try {
            Statement stm = null;
            stm = con.createStatement();
            stm.executeUpdate(query);
            return true;

        } catch (Exception e) {
            System.out.println("ha fallado update");
            System.out.println(e.toString());
            return false;
        }
    }

}
