package com.equipodelta.link.it.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class DAO<T> {

    protected Connection con;

    private String IDCol;
    private String table;
    private String colums;

    public DAO(String table, String id, String co) {
        try {
            this.con = new Conection().ConnectionJDBC();

        } catch (Exception ex) {
            System.out.println("Error stablishing connection to db");
            System.out.println(ex);
        }
        this.table = table;
        this.IDCol = id;
        this.colums = co;
    }

    protected abstract T build(ResultSet rs) throws SQLException;

    protected abstract String[] deconstruct(T o);

    protected ResultSet query(String query) {
        System.out.println("query:\n" + query);
        try {
            Statement stm = null;
            ResultSet rs = null;

            stm = con.createStatement();
            rs = stm.executeQuery(query);
            return rs;
        } catch (SQLException ex) {
            System.out.println("Error en la query");
            System.out.println(ex);
        }
        return null;
    }

    public T getOne(String id) {

        try {
            String query = String.format("SELECT * FROM %s WHERE %s=\"%s\";", table, IDCol, id);

            ResultSet rs = query(query);

            if (rs.next()) {
                System.out.println("Get one: existe");
                return build(rs);
            }
            System.out.println("GetOne: no existe");
            return null;
        } catch (SQLException ex) {
            System.out.println("GetOne: Peta");
            System.out.println(ex);
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("GetOne: final");
        return null;

    }

    public List<T> getAll() throws SQLException {
        //Returns all data in $table
        return buildList(query("SELECT * FROM " + table));
    }

    public boolean insert(T o) {
        StringBuilder sb = new StringBuilder();
        for (String s : deconstruct(o)) {
            sb.append("\", \"" + s);
        }
//		String query = String.format("INSERT INTO %s VALUES (%s);", table, sb.substring(2));
        String query = String.format("INSERT INTO `linkitdatabase`.`%s` %s VALUES (%s\");", table, colums, sb.substring(3));
        System.out.println(query);
        try {
            Statement stm = null;
            stm = con.createStatement();
            stm.executeUpdate(query);
            return true;

        } catch (Exception e) {
            System.out.println("ha fallado insert");
            System.out.println(e.toString());
            return false;
        }

    }

    public void delete(String ID) {

        String query = String.format("DELETE FROM %s WHERE %s = \"%s\";", table, IDCol, ID);
        try {
            Statement stm = null;

            stm = con.createStatement();
            stm.executeUpdate(query);

        } catch (Exception e) {
            System.out.println("ha fallado delete");
        }

    }

    public boolean update(T o) {
        delete(deconstruct(o)[0]);
        insert(o);
        return true;
    }

    protected List<T> buildList(ResultSet rs) throws SQLException {
        List<T> list = new ArrayList<>();

        while (rs.next()) {
            list.add(build(rs));
        }
        return list;
    }

    public boolean exist(String code) {
        return getOne(code) != null;
    }
}
