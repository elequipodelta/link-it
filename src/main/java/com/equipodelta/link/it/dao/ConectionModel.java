package com.equipodelta.link.it.dao;

import java.util.Objects;

public class ConectionModel{
    
    private String db_ = "linkitdatabase";
    private String login_ = "root";
    private String password_ = "admin";
    private String url_ = "jdbc:mysql://localhost:3306/linkitdatabase?serverTimezone=UTC";
    
    public ConectionModel(){
        
    }
    
    public ConectionModel(String db, String login, String password, String url){
        this.db_=db;
        this.login_=login;
        this.password_=password;
        this.url_=url;
    }

    public String getDb() {
        return db_;
    }

    public void setDb(String db) {
        this.db_ = db;
    }

    public String getLogin() {
        return login_;
    }

    public void setLogin(String login) {
        this.login_ = login;
    }

    public String getPassword() {
        return password_;
    }

    public void setPassword(String password) {
        this.password_ = password;
    }

    public String getUrl() {
        return url_;
    }

    public void setUrl(String url) {
        this.url_ = url;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.db_);
        hash = 37 * hash + Objects.hashCode(this.login_);
        hash = 37 * hash + Objects.hashCode(this.password_);
        hash = 37 * hash + Objects.hashCode(this.url_);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        return true;
    }
    
}
