package com.equipodelta.link.it.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.equipodelta.link.it.models.User;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDAO extends DAO<User> {

    public UserDAO() {
        super("users", "CODIGO", "(`EMAIL`, `NICK`, `CODIGO`)");
        // TODO Auto-generated constructor stub
    }

    @Override
    protected User build(ResultSet rs) throws SQLException {
        // TODO Auto-generated method stub
        User user = new User();
        user.setId(rs.getString("CODIGO"));
        user.setEmail(rs.getString("EMAIL"));
        user.setUsername(rs.getString("NICK"));

        return user;
    }

    @Override
    protected String[] deconstruct(User o) {
        String[] atr = new String[3];
        atr[0] = o.getEmail();
        atr[1] = o.getUsername();
        atr[2] = o.getId();
        return atr;
    }

    public String generateCode() {
        //Podria ser peor la verdad🤷
        Random rdm = new Random();
        String code = "u" + (Math.abs(rdm.nextInt()) % 10000);
        if (!exist(code)) {
            return code;
        } else {
            return generateCode();
        }

    }

    public String getIDByUsername(String username) {

        try {
            String query = String.format("SELECT CODIGO FROM users WHERE NICK=\"%s\"", username);
            System.out.println("query getIDByUsername: " + query);
            ResultSet rs = query(query);

            if (rs.next()) {
                return rs.getString("CODIGO");
            }
            return "";
        } catch (SQLException ex) {
            System.out.println(ex);
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";

    }
}
