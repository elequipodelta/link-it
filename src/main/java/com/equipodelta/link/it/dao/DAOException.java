package com.equipodelta.link.it.dao;

/**
 * Creation of a custom exception to treat errors related to the Data Access Object design pattern.
 * It has to be thrown to be used accordingly.
 */
public class DAOException extends Exception {

    private static final long serialVersionUID = 3192286437244224083L;

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }
}
