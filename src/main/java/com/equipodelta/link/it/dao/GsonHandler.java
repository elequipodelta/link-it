/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equipodelta.link.it.dao;

import com.equipodelta.link.it.models.Card;
import com.equipodelta.link.it.models.CardText;
import com.equipodelta.link.it.models.Deck;
import com.equipodelta.link.it.models.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Aimnox
 */
public abstract class GsonHandler {

    private static final RuntimeTypeAdapterFactory<Card> runtimeTypeAdapterFactory
            = RuntimeTypeAdapterFactory.of(Card.class, "cardType")
                    .registerSubtype(CardText.class, "TEXT");

    private static final Gson gson = new GsonBuilder().setPrettyPrinting()
            .registerTypeAdapterFactory(runtimeTypeAdapterFactory).create();

    private static BufferedWriter writer;

    public static void toFile(Object o, String file) throws IOException {
        writer = new BufferedWriter(new FileWriter(file));
        writer.write(toJsonString(o));
        writer.close();
    }

    public static String toJsonString(Object o) {
        String s = gson.toJson(o);
        return s;
    }

    public static Deck loadDeck(String file) throws FileNotFoundException {
        return gson.fromJson(new FileReader(file), Deck.class);
    }

    public static User loadUser(String file) throws FileNotFoundException {
        return gson.fromJson(new FileReader(file), User.class);

    }

}
