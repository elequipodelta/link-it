/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equipodelta.link.it.controllers;

import com.equipodelta.link.it.models.Deck;

/**
 *
 * @author Aimnox
 */
public class DeckController {

    private Deck deck;

    public DeckController() {
        this.deck = new Deck();
    }

    public DeckController(Deck deck) {
        this.deck = deck;
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

}
