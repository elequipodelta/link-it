/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equipodelta.link.it.controllers;

import com.equipodelta.link.it.models.User;

/**
 *
 * @author David
 */
public class UserController {
   private User user;

    public UserController() {
        this.user= new User();
    }

    public UserController(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
}
