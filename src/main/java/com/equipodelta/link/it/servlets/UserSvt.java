/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equipodelta.link.it.servlets;

import com.equipodelta.link.it.dao.Conection;
import com.equipodelta.link.it.dao.DecksDAO;
import com.equipodelta.link.it.dao.UserDAO;
import com.equipodelta.link.it.models.Card;
import com.equipodelta.link.it.models.CardText;
import com.equipodelta.link.it.models.Deck;
import com.equipodelta.link.it.models.User;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mtt99
 */
@WebServlet(name = "UserSvt", urlPatterns = {"/user"})
public class UserSvt extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response)

        User user;
        String nick = request.getParameter("u");
        UserDAO daoU = new UserDAO();
        DecksDAO daoD = new DecksDAO();
        String id = daoU.getIDByUsername(nick);
        System.out.println(nick + " holaa " + id);
        Map<String, Deck> map = new HashMap<>();
        for (Deck d : daoD.getDeckByUser(id)) {
            map.put(d.getId(), d);
        }

        user = daoU.getOne(id);

        user.setDecks(map);

        request.setAttribute("user", user);

        RequestDispatcher rd = request.getRequestDispatcher("user.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println(request.getParameter("title"));
        UserDAO daoU = new UserDAO();
        User u1 = new User();
        u1.setEmail(request.getParameter("email"));
        u1.setUsername(request.getParameter("name"));
        u1.setId(daoU.generateCode());
        daoU.insert(u1);

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
