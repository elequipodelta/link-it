/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equipodelta.link.it.servlets;

import com.equipodelta.link.it.dao.CartasDAO;
import com.equipodelta.link.it.dao.DecksDAO;
import com.equipodelta.link.it.models.Card;
import com.equipodelta.link.it.models.CardText;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Aimnox
 */
public class CardSvt extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CardSvt</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CardSvt at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold  desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        System.out.println("eiiiii" + request.getParameter("action"));
        CartasDAO dao = new CartasDAO();
        switch (request.getParameter("action")) {
            
            case "AD":
                Card card = new CardText();
                card.setId(dao.generateCode(request.getParameter("did"), 1));
                card.setTitle(request.getParameter("title"));
                card.setDesc(request.getParameter("desc"));
                card.setDeck(new DecksDAO().getOne(request.getParameter("did")));
                dao.insert(card);
                break;
            case "DEL":
                System.out.println("eiiiii" + request.getParameter("action"));
                System.out.println(request.getParameter("cardId"));
                dao.delete(request.getParameter("cardId"));
                break;

            default:
            // code block
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
