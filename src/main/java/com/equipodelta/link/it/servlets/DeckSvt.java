/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equipodelta.link.it.servlets;

import com.equipodelta.link.it.dao.CartasDAO;
import com.equipodelta.link.it.dao.DecksDAO;
import com.equipodelta.link.it.dao.UserDAO;
import com.equipodelta.link.it.models.Card;
import com.equipodelta.link.it.models.Deck;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Aimnox
 */
@WebServlet(name = "DeckSvt", urlPatterns = {"/Deck"})
public class DeckSvt extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Deck deck1 = new DecksDAO().getOne(request.getParameter("did"));

        Map<String, Card> cards = new HashMap<>();
        List<Card> cartas = new CartasDAO().getCartasByDeck(request.getParameter("did"));
        for (Card c : cartas) {
            cards.put(c.getId(), c);
        }
        System.out.println(deck1);
        deck1.setCards(cards);
        deck1.loadSerializedLinks();
        System.out.println("Cartas cargadas:" + cards.size());
        request.setAttribute("deck", deck1);
        request.setAttribute("cartas", deck1.getCards().values());
        request.setAttribute("links", deck1.getSerializedLinks());
        RequestDispatcher rd = request.getRequestDispatcher("deck.jsp");
        rd.forward(request, response);
        // processRequest(request, respons  e);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        switch (request.getParameter("action")) {
            case "AD":
                addDeck(request, response);
                break;
            case "AL":
                addLink(request, response);
                break;
            case "RL":
                removeLink(request, response);
                break;
            case "DD":
                removeDeck(request, response);
            default:
            // code block
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void addDeck(HttpServletRequest request, HttpServletResponse response) {
        DecksDAO daoD = new DecksDAO();
        Deck deck = new Deck();
        System.out.println(request.getParameter("uid").toString());
        System.out.println("pepe");
        deck.setId(daoD.generateCode(request.getParameter("uid")));
        deck.setUser(new UserDAO().getOne(request.getParameter("uid")));
        deck.setTitle(request.getParameter("title"));

        daoD.insert(deck);
    }

    private void addLink(HttpServletRequest request, HttpServletResponse response) {
        DecksDAO daoD = new DecksDAO();
        String link = request.getParameter("card1") + "-" + request.getParameter("card2");
        System.out.println("holaala   "+request.getParameter("card1") + "-" + request.getParameter("card2"));
        Deck deck = daoD.getOne(request.getParameter("did"));

        Map<String, Card> cards = new HashMap<>();
        List<Card> cartas = new CartasDAO().getCartasByDeck(request.getParameter("did"));
        for (Card c : cartas) {
            cards.put(c.getId(), c);
        }
        deck.setCards(cards);
        deck.loadSerializedLinks();
        deck.addLink(link);
        daoD.update(deck);
    }

    private void removeLink(HttpServletRequest request, HttpServletResponse response) {
        DecksDAO daoD = new DecksDAO();
        String link = request.getParameter("card1") + "-" + request.getParameter("card2");
        Deck deck = daoD.getOne(request.getParameter("did"));

        Map<String, Card> cards = new HashMap<>();
        List<Card> cartas = new CartasDAO().getCartasByDeck(request.getParameter("did"));
        for (Card c : cartas) {
            cards.put(c.getId(), c);
        }
        deck.setCards(cards);
        deck.loadSerializedLinks();
        deck.removeLink(link);
        daoD.update(deck);
    }

    private void removeDeck(HttpServletRequest request, HttpServletResponse response) {

        DecksDAO dao = new DecksDAO();
        dao.delete(request.getParameter("did"));




    }
}
